import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TourlistComponent } from './tourlist/tourlist.component';
const routes: Routes = [
  {path: 'tour', component: TourlistComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TourRoutingModule { }


// import { Component, OnInit } from '@angular/core';
// import { BlogpostService } from '../blogpost.service';
// import { Blogpost } from '../blogpost';
// import { Title } from '@angular/platform-browser';

// @Component({
//   selector: 'app-blogpost-list',
//   templateUrl: './blogpost-list.component.html',
//   styleUrls: ['./blogpost-list.component.css']
// })
// export class BlogpostListComponent implements OnInit {

//   title = 'Blogs';
//   blogs: Blogpost;
//   error: {};
//   UserData:any;
//   constructor(
//     private titleService: Title,
//     private blogpostService: BlogpostService
//     ) {

      
//      }

//   ngOnInit() {
// this.allbloglist1();
//   }

//   allbloglist1(){
//      this.blogpostService.allblogger().then((result)=>{
//       // console.log('blogger list',result);
//       this.UserData =result
//      console.log('blogger list',this.UserData);
//  }, (err) => {
//  console.log(err);
 
//  });
 
//    }

//    allbloglist() {
//     this.blogpostService.getFeaturedBlogs().subscribe(
//       (data: Blogpost) => this.blogs = data,
//       error => this.error = error
//     );
//   }
// }


import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../blogpost.service';
import { Blogpost } from '../blogpost';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-blogpost-list',
  templateUrl: './blogpost-list.component.html',
  styleUrls: ['./blogpost-list.component.css']
})
export class BlogpostListComponent implements OnInit {

  title = 'Blogs';
 // blogs: Blogpost;
 blogs:any;
 UserData:any;
  error: {};

  constructor(
    private titleService: Title,
    private blogpostService: BlogpostService
    ) { 
      
    }

  ngOnInit() {
    this.allblogListData();
  
  }

  allblogListData(){
    this.titleService.setTitle(this.title);
    this.blogpostService.getBlogs().subscribe(data =>{
      this.blogs = data
    },
      error => this.error = error
    );
  }

  level()
  {   
      this.blogpostService.getBlogs()
        .subscribe(res => {
          this.blogs = res
        
        }, (err) => {
         
          error => this.error = error
        });
  
  }

    allbloglist1(){
     this.blogpostService.allblogger().then((result)=>{
      // console.log('blogger list',result);
      this.UserData =result
     console.log('blogger list',this.UserData);
 }, (err) => {
 console.log(err);
 
 });
 
   }
}


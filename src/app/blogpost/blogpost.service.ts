
import { Injectable } from '@angular/core';
import { Blogpost } from './blogpost';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogpostService {

  //ServerUrl = 'http://localhost:3001/';
  ServerUrl = 'http://3.134.218.36/';
  errorData: {};
    
  constructor(private http: HttpClient) { }

  getBlogs() {
    return this.http.get(this.ServerUrl + 'blog/all').pipe(
      catchError(this.handleError)
    );
  }

  getcategories() {
    return this.http.get(this.ServerUrl + 'cat/all').pipe(
      catchError(this.handleError)
    );
  }
  recentBlog() {
    return this.http.get(this.ServerUrl + 'Main/all').pipe(
      catchError(this.handleError)
    );
  }
  SubCategory(id) {
    return this.http.post(this.ServerUrl + 'img/findById',id).pipe(
      catchError(this.handleError)
    );
  }
  Blogfeatured() {
    return this.http.get(this.ServerUrl + 'blog/all2').pipe(
      catchError(this.handleError)
    );
  }
  getBlog(id: number) {
    return this.http.get<Blogpost>(this.ServerUrl + 'blog/' + id)
    .pipe(
      catchError(this.handleError)
    );
}
  allblogger()
  {    
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:3001/blog/all')
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(this.handleError);
        });
    });
  }

  getBlogDetails(id)
  {  
    return new Promise((resolve, reject) => {
      this.http.post('http://localhost:3001/blog/getdetails', id)
      
        .subscribe(res => {
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
 
  getFeaturedBlogs() {
    return this.http.get<Blogpost>(this.ServerUrl + 'api/featured_blogs').pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong,

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message

    this.errorData = {
      errorTitle: 'Oops! Request for document failed',
      errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
  }
}

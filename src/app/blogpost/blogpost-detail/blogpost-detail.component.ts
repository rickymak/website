
    import { Component, OnInit } from '@angular/core';
    import { Router, ActivatedRoute, ParamMap} from '@angular/router';
    import { BlogpostService } from '../blogpost.service';
    import { Blogpost } from '../blogpost';
    import { Title } from '@angular/platform-browser';
    import { Observable } from 'rxjs';
    import { throwError } from 'rxjs';
    
    @Component({
      selector: 'app-blogpost-detail',
      templateUrl: './blogpost-detail.component.html',
      styleUrls: ['./blogpost-detail.component.css']
    })
    export class BlogpostDetailComponent implements OnInit {

      blog$: Observable<Blogpost>;
      details:any;     
      title:any
    SubCategory:any;
    blogpostRecent:any;
    error:{}
      constructor(
        private route: ActivatedRoute,
        private router: Router,
        private blogpostService: BlogpostService,
        private titleService: Title
        ) { }

      // ngOnInit() {
      //   this.blog$ = this.route.paramMap.pipe(
      //     switchMap((params: ParamMap) =>
      //       this.blogpostService.getBlog(+params.get('id'))
      //     )
      //   );

      //   this.titleService.setTitle('Blog Detail');
      // }

      ngOnInit(){       
         this.details = this.route.params.subscribe(params => {       
       this.getdetails(params.id)
            });
            this.allblogListData();
            }

   getdetails(user_id )
  {
    let postParams = {
    
      postId:user_id
     }
     this.blogpostService.SubCategory(postParams).subscribe(data =>{
      this.SubCategory = data['data'];
      console.log('this.SubCategory>>>>>>>',this.SubCategory)  
    },
      error => this.error = error
    );


  }
  allblogListData(){
    this.blogpostService.recentBlog().subscribe(data =>{
      this.blogpostRecent = data['data']
    },
      error => this.error = error
    );
  }
  OpenPage(id){
 console.log('ID>>>>>',id)
  }

    }

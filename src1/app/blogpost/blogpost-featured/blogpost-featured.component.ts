import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../blogpost.service';
@Component({
  selector: 'app-blogpost-featured',
  templateUrl: './blogpost-featured.component.html',
  styleUrls: ['./blogpost-featured.component.css']
})
export class BlogpostFeaturedComponent implements OnInit {
  mainCategory:any;
  error:{}
  constructor(private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.allblogListData();
  }
  allblogListData(){
    this.blogpostService.recentBlog().subscribe(data =>{
      this.mainCategory = data['data'];
      // alert(JSON.stringify(this.mainCategory))   
    },
      error => this.error = error
    );
  }

}

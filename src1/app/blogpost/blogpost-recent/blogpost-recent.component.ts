import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../blogpost.service';
@Component({
  selector: 'app-blogpost-recent',
  templateUrl: './blogpost-recent.component.html',
  styleUrls: ['./blogpost-recent.component.css']
})
export class BlogpostRecentComponent implements OnInit {
  blogpostRecent:any;
  error:{};
  constructor(private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.allblogListData();
  }
  allblogListData(){
    this.blogpostService.recentBlog().subscribe(data =>{
      this.blogpostRecent = data['data']
    },
      error => this.error = error
    );
  }
}

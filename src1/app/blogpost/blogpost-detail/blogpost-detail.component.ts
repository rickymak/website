
    import { Component, OnInit } from '@angular/core';
    import { Router, ActivatedRoute, ParamMap} from '@angular/router';
    import { BlogpostService } from '../blogpost.service';
    import { Blogpost } from '../blogpost';
    import { Title } from '@angular/platform-browser';
    import { Observable } from 'rxjs';
    import { throwError } from 'rxjs';
    
    @Component({
      selector: 'app-blogpost-detail',
      templateUrl: './blogpost-detail.component.html',
      styleUrls: ['./blogpost-detail.component.css']
    })
    export class BlogpostDetailComponent implements OnInit {

      blog$: Observable<Blogpost>;
      details:any
      id:any
      errorData:any
      author:any
      title:any
      short_desc:any
      created_at:any
      constructor(
        private route: ActivatedRoute,
        private router: Router,
        private blogpostService: BlogpostService,
        private titleService: Title
        ) { }

      // ngOnInit() {
      //   this.blog$ = this.route.paramMap.pipe(
      //     switchMap((params: ParamMap) =>
      //       this.blogpostService.getBlog(+params.get('id'))
      //     )
      //   );

      //   this.titleService.setTitle('Blog Detail');
      // }

      ngOnInit(){       
         this.details = this.route.params.subscribe(params => {
           alert(params.id)
       this.getdetails(params.id)
            });
            }

   getdetails(user_id )
  {
    let postParams = {
    
     id:user_id
     }
   this.blogpostService.getBlogDetails(postParams).then((result)=>{
    // alert(JSON.stringify(result['created_at']))
    this.author= result['author'];
    this.title= result['title'];
    this.short_desc= result['short_desc'];
    this.created_at= result['created_at']
     
    }, (err) => { 
      if (err.status == 404) {
        alert(JSON.stringify(err.status ))
        this.errorData = {
          errorTitle: 'Oops! Request for document failed',
          errorDesc: 'Something bad happened. Please try again later.'
        };
        return throwError(this.errorData);
      } 

    
// });
  });


  }

    }

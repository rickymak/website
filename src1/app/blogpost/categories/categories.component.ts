import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../blogpost.service';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories:any
  error: {};
  constructor(  private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.allblogListData();
  }
  allblogListData(){
   
    this.blogpostService.getcategories().subscribe(data =>{
      this.categories = data
    },
      error => this.error = error
    );
  }
}
